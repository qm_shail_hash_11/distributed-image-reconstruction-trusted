import cvxpy as cp
import numpy as np
import pprint as pp
from scipy import signal as sp
import matplotlib.pyplot as plt

def get_indices(n, num):
    #np.random.seed(234923)
    omega = np.sort(np.random.permutation(np.arange(n*n))[:num])
    indices = np.unravel_index(omega, (n,n))
    print((indices[0]))
    return indices

# Delete data from x
def remove_data(x, number_of_samples):
    n = x.shape[0]
    y = np.empty((n,n))
    y[:] = np.nan
    indices = get_indices(n, number_of_samples)
    y[indices] = x[indices]
    return y, indices

# Generate data
def get_data(n, rank, number_of_samples):
    np.random.seed(2236)
    max_int = 5
    a = np.random.randint(max_int, size=(n, rank))
    b = np.random.randint(max_int, size=(rank, n))
    x = a @ b
    obs_mat, indices = remove_data(x, number_of_samples)
    return x, indices

# Generate Problem
def complete_mat_sdp(x, indices, show=False):
    n = x.shape[0]
    xk = cp.Variable(shape=(n,n))
    obj = cp.Minimize(cp.norm(xk, "nuc"))
    constraints = [
    xk[indices] == x[indices]
    ]
    prob = cp.Problem(obj, constraints)
    prob.solve()
    x = np.rint(x)
    xk = np.rint(xk.value)
    if show:
        print("status:", prob.status)
        print("The optimal value is", prob.value)
        print(x)
        print(xk)
        print(np.linalg.norm(x-xk)/n)
    #print(x-xk)
    return xk

# For single n, rank, number of samples
n = 10
rank = 3
number_of_samples = int(0.5*n*n)
x, indices = get_data(n, rank, number_of_samples)
x_pred = complete_mat_sdp(x, indices, True)
