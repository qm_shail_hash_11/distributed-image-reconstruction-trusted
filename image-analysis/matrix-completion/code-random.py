import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
from scipy.sparse import lil_matrix
import os
import time
import cvxpy as cp
import pprint as pp
from scipy import signal as sp
import random
from datetime import datetime
import math

#RPCA Implementation
################################################################################################################

def shrink(X, tau):
    output = np.sign(X) * np.maximum((np.abs(X) - tau), np.zeros(X.shape))
    return output

def svd_threshold(M, tau):
    U, S, V = np.linalg.svd(M, full_matrices=False)
    output = np.dot(U, np.dot(np.diag(shrink(S, tau)), V))
    return output

def RPCA(X):
    n = X.shape
    mu = n[0]*n[1] / (4 * np.sum(X))
    l = 1/np.sqrt(np.max(n))
    threshold = 1e-7*np.linalg.norm(X,'fro')
    L = np.zeros(X.shape)
    S = np.zeros(X.shape)
    Y = np.zeros(X.shape)
    count = 0

    while ((np.linalg.norm(X-L-S,'fro')>threshold) and (count<1000)):
        L = svd_threshold(X-S+(1/mu)*Y,1/mu);
        S = shrink(X-L+(1/mu)*Y,l/mu);
        Y = Y + mu*(X-L-S);
        count = count + 1
    return [L, S]
################################################################################################################

# Getting the distance between two matrices
################################################################################################################
# Get difference between two matrices
def getDivMat(mat1, mat2):
    s = mat1.shape
    mat3 = []
    for i in range(s[0]):
        mat4 = []
        for j in range(s[1]):
            mat4.append(mat1[i][j]-mat2[i][j])
        mat3.append(mat4)
    mat3 = np.array(mat3)
    return mat3

def getMatDist(mat):
    s = mat.shape
    dist = 0
    for i in range(s[0]):
        for j in range(s[1]):
            dist = dist + mat[i][j] * mat[i][j]
    return dist

################################################################################################################

# Matrix stacking and destacking
################################################################################################################

def stackImage(img):
    s = img.shape
    mat1 = []
    for k in range(s[2]):
        mat2 = []
        for i in range(s[0]):
            mat3 = []
            for j in range(s[1]):
                mat3.append(img[i][j][k])
            mat2.append(mat3)
        mat1.append(mat2)

    mat1 = np.array(mat1)

    matfinal = []

    for i in range(mat1.shape[0]):
        for j in range(mat1.shape[1]):
            matfinal.append(mat1[i][j])

    matfinal = np.array(matfinal)
    return matfinal

def destackImage(img):
    s = img.shape
    num1 = int(s[0]/3)
    mat1 = []
    for i in range(num1):
        mat2 = []
        for j in range(s[1]):
            mat3 = []
            mat3.append(img[i][j])
            mat3.append(img[num1+i][j])
            mat3.append(img[2*num1+i][j])
            mat2.append(mat3)
        mat1.append(mat2)

    mat1 = np.array(mat1)
    return mat1

################################################################################################################

################################################################################################################

# Delete data from x
def remove_rows(x, total, num):
    random.seed(time.time())
    np.random.seed(random.randint(1, 10000))
    indices = []
    if num == 0:
        return x, indices

    rows = np.random.randint(low = 0, high = total, size=num)
    for i in range(len(rows)):
        x[rows[i]] = [0 for t in range(x.shape[0])]

    ivalues = []
    jvalues = []

    for i in range(x.shape[0]):
        flag = 0
        for r in rows:
            if i == r:
                flag = 1
                break
        if flag == 1:
            continue
        for j in range(x.shape[1]):
            ivalues.append(i)
            jvalues.append(j)

    indices = (ivalues, jvalues)

    # print(x)
    # print(indices)

    return x, indices

def complete_mat_sdp(x, indices, show=False):
    if len(indices) == 0:
        return x
    n = x.shape[0]
    xk = cp.Variable(shape=(n,n))
    obj = cp.Minimize(cp.norm(xk, "nuc"))
    constraints = [
    xk[indices] == x[indices]
    ]
    prob = cp.Problem(obj, constraints)
    prob.solve()
    x = np.rint(x)
    xk = np.rint(xk.value)
    if show:
        print("status:", prob.status)
        print("The optimal value is", prob.value)
        print(x)
        print(xk)
        print(np.linalg.norm(x-xk)/n)
    #print(x-xk)
    return xk

################################################################################################################

def main():
    denoiserName = "rpca"
    dirname = "./" + denoiserName
    isdir = os.path.isdir(dirname)
    if isdir == False:
        os.mkdir(dirname)

    imgfile = "./aa.jpeg"
    img = cv2.imread(imgfile)
    stackedImage = stackImage(img)
    X = stackedImage

    dim = X.shape
    num1 = int(dim[0]/3)
    errnums = 1
    errmag = 30
    maxelimination = 30
    # Generate random noise and then, making the matrix sparse
    for errnum in range(errnums):
        errorname = "none"
        errormagnitude = []
        timetaken = []
        normdistances = []
        distances = []
        Xerr = np.array([ele[:] for ele in X])
        if errnum == 0:
            A = np.random.randint(errmag, size=(dim[0], dim[1]))
            Err = lil_matrix(A).toarray()
            Xerr = X + Err
            Xerr = np.clip(Xerr, 0, 255)
            errorname = "random"
        # Gaussian Noise
        elif errnum == 1:
            mean = 0.0
            std = errmag
            Xerr = X + np.random.normal(mean, std, X.shape)
            Xerr = np.clip(Xerr, 0, 255)
            errorname = "gaussian"
        # Poisson Noise
        elif errnum == 2:
            vals = len(np.unique(Xerr))
            vals = 2 ** np.ceil(np.log2(vals)) * errmag
            Xerr = np.random.poisson(Xerr * vals) / float(vals)
            errorname = "poisson"
        # Speckle Noise
        elif errnum == 3:
            Err = errmag * np.random.randn(X.shape[0], X.shape[1])
            Err = Err.reshape(X.shape)
            Xerr = X + X * Err
            Xerr = np.clip(Xerr, 0, 255)
            errorname = "speckle"

        for i in range(1, maxelimination):

            numrowsdel = 5 * i

            Xerr_R_a = Xerr[0:num1]
            Xerr_G_a = Xerr[num1:2*num1]
            Xerr_B_a = Xerr[2*num1:3*num1]

            a = int(numrowsdel/3)
            b = numrowsdel%3
            inds = []
            for _ in range(3):
                inds.append(a)
            if b == 1:
                inds[0] = inds[0] + 1
            if b == 2:
                inds[0] = inds[0] + 1
                inds[1] = inds[1] + 1

            Xerrdrop_R, indices_R = remove_rows(Xerr_R_a, num1, inds[0])
            Xerrdrop_G, indices_G = remove_rows(Xerr_G_a, num1, inds[1])
            Xerrdrop_B, indices_B = remove_rows(Xerr_B_a, num1, inds[2])

            begin = time.time()

            [Ldash_R, S_R] = RPCA(Xerrdrop_R)
            [Ldash_G, S_G] = RPCA(Xerrdrop_G)
            [Ldash_B, S_B] = RPCA(Xerrdrop_B)

            L_R = complete_mat_sdp(Ldash_R, indices_R)
            L_G = complete_mat_sdp(Ldash_G, indices_G)
            L_B = complete_mat_sdp(Ldash_B, indices_B)

            end = time.time()

            filteredImage_R = L_R.astype(np.uint8)
            filteredImage_G = L_G.astype(np.uint8)
            filteredImage_B = L_B.astype(np.uint8)


            distr = np.linalg.norm((filteredImage_R-X[0:num1])/num1)
            distg = np.linalg.norm((filteredImage_G-X[num1:2*num1])/num1)
            distb = np.linalg.norm((filteredImage_R-X[2*num1:3*num1])/num1)
            errsum = (distr*distr + distg * distg + distb*distb)/3
            normdist = math.sqrt(errsum)

            errormagnitude.append(numrowsdel)
            timetaken.append(end-begin)
            normdistances.append(normdist)

            print(i)

        filename = dirname + "/" + errorname + "-random.csv"
        dict1 = {'Error': errormagnitude, 'Time taken': timetaken, 'Distance': normdistances}
        df1 = pd. DataFrame(dict1)
        df1.to_csv(filename, mode='w', index=False)


if __name__ == '__main__':
    main()
