import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
from scipy.sparse import lil_matrix
import os
import time
import cvxpy as cp
import pprint as pp
from scipy import signal as sp
import random
from datetime import datetime
import math
import copy

# Matrix stacking and destacking
################################################################################################################

def stackImage(img):
    s = img.shape
    mat1 = []
    for k in range(s[2]):
        mat2 = []
        for i in range(s[0]):
            mat3 = []
            for j in range(s[1]):
                mat3.append(img[i][j][k])
            mat2.append(mat3)
        mat1.append(mat2)

    mat1 = np.array(mat1)

    matfinal = []

    for i in range(mat1.shape[0]):
        for j in range(mat1.shape[1]):
            matfinal.append(mat1[i][j])

    matfinal = np.array(matfinal)
    return matfinal

def destackImage(img):
    s = img.shape
    num1 = int(s[0]/3)
    mat1 = []
    for i in range(num1):
        mat2 = []
        for j in range(s[1]):
            mat3 = []
            mat3.append(img[i][j])
            mat3.append(img[num1+i][j])
            mat3.append(img[2*num1+i][j])
            mat2.append(mat3)
        mat1.append(mat2)

    mat1 = np.array(mat1)
    return mat1

################################################################################################################

################################################################################################################

# Delete data from x
def remove_rows(x, total, num):
    random.seed(time.time())
    np.random.seed(random.randint(1, 10000))
    indices = []
    if num == 0:
        return x, indices

    rows = np.random.randint(low = 0, high = total, size=num)
    for i in range(len(rows)):
        x[rows[i]] = [0 for t in range(x.shape[0])]

    ivalues = []
    jvalues = []

    for i in range(x.shape[0]):
        flag = 0
        for r in rows:
            if i == r:
                flag = 1
                break
        if flag == 1:
            continue
        for j in range(x.shape[1]):
            ivalues.append(i)
            jvalues.append(j)

    indices = (ivalues, jvalues)

    # print(x)
    # print(indices)

    return x, indices

def complete_mat_sdp(x, indices, show=False):
    if len(indices) == 0:
        return x
    n = x.shape[0]
    xk = cp.Variable(shape=(n,n))
    obj = cp.Minimize(cp.norm(xk, "nuc"))
    constraints = [
    xk[indices] == x[indices]
    ]
    prob = cp.Problem(obj, constraints)
    prob.solve()
    x = np.rint(x)
    xk = np.rint(xk.value)
    if show:
        print("status:", prob.status)
        print("The optimal value is", prob.value)
        print(x)
        print(xk)
        print(np.linalg.norm(x-xk)/n)
    #print(x-xk)
    return xk

################################################################################################################

def main():
    random.seed(time.time())
    implname = "privacy"
    dirname = "./" + implname
    isdir = os.path.isdir(dirname)
    if isdir == False:
        os.mkdir(dirname)

    imgfile = "./aa.jpeg"
    img = cv2.imread(imgfile)
    stackedImage = stackImage(img)
    X = stackedImage

    dim = X.shape
    num1 = int(dim[0]/3)
    errnums = 4
    errmag = 1
    maxelimination = int(dim[0]/30)

    for t in range(25, 30):

        removedrows = []
        normdistances = []
        times = []


        for i in range(20*t, 20*(t+1)):

            print(i)

            rownums = random.sample(range(1, dim[0]), i)

            Xerr = np.array([ele[:] for ele in X])

            Xerr_R_a = Xerr[0:num1]
            Xerr_G_a = Xerr[num1:2*num1]
            Xerr_B_a = Xerr[2*num1:3*num1]

            a = int(i/3)
            b = i%3
            inds = []
            for j in range(3):
                inds.append(a)
            if b == 1:
                inds[0] = inds[0] + 1
            if b == 2:
                inds[0] = inds[0] + 1
                inds[1] = inds[1] + 1

            Xerrdrop_R, indices_R = remove_rows(Xerr_R_a, num1, inds[0])
            Xerrdrop_G, indices_G = remove_rows(Xerr_G_a, num1, inds[1])
            Xerrdrop_B, indices_B = remove_rows(Xerr_B_a, num1, inds[2])

            begin = time.time()

            Xerr_R = complete_mat_sdp(Xerrdrop_R, indices_R)
            Xerr_G = complete_mat_sdp(Xerrdrop_G, indices_G)
            Xerr_B = complete_mat_sdp(Xerrdrop_B, indices_B)

            Xerr_R = Xerr_R.astype(np.uint8)
            Xerr_G = Xerr_G.astype(np.uint8)
            Xerr_B = Xerr_B.astype(np.uint8)

            end = time.time()

            t1 = end - begin

            distr = np.linalg.norm((Xerr_R-X[0:num1])/num1)
            distg = np.linalg.norm((Xerr_G-X[num1:2*num1])/num1)
            distb = np.linalg.norm((Xerr_B-X[2*num1:3*num1])/num1)
            errsum = (distr*distr + distg * distg + distb*distb)/3
            normdist = math.sqrt(errsum)
            removedrows.append(i)
            normdistances.append(normdist)
            times.append(t1)

        filename = dirname + "/" + "magnitude-" + str(t) + ".csv"
        print(removedrows)
        print(normdistances)
        dict1 = {'No. of rows removed': removedrows, 'Distance': normdistances, 'Time': times}
        df1 = pd. DataFrame(dict1)
        df1.to_csv(filename, mode='w', index=False)


if __name__ == '__main__':
    main()
