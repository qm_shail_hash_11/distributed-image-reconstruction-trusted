import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
from scipy.sparse import lil_matrix
import os
import time
from scipy.signal.signaltools import wiener
import math

# Getting the distance between two matrices
################################################################################################################
# Get difference between two matrices
def getDivMat(mat1, mat2):
    s = mat1.shape
    mat3 = []
    for i in range(s[0]):
        mat4 = []
        for j in range(s[1]):
            mat4.append(mat1[i][j]-mat2[i][j])
        mat3.append(mat4)
    mat3 = np.array(mat3)
    return mat3

def getMatDist(mat):
    s = mat.shape
    dist = 0
    for i in range(s[0]):
        for j in range(s[1]):
            dist = dist + mat[i][j] * mat[i][j]
    return dist

################################################################################################################

# Matrix stacking and destacking
################################################################################################################

def stackImage(img):
    s = img.shape
    mat1 = []
    for k in range(s[2]):
        mat2 = []
        for i in range(s[0]):
            mat3 = []
            for j in range(s[1]):
                mat3.append(img[i][j][k])
            mat2.append(mat3)
        mat1.append(mat2)

    mat1 = np.array(mat1)

    matfinal = []

    for i in range(mat1.shape[0]):
        for j in range(mat1.shape[1]):
            matfinal.append(mat1[i][j])

    matfinal = np.array(matfinal)
    return matfinal

def destackImage(img):
    s = img.shape
    num1 = int(s[0]/3)
    mat1 = []
    for i in range(num1):
        mat2 = []
        for j in range(s[1]):
            mat3 = []
            mat3.append(img[i][j])
            mat3.append(img[num1+i][j])
            mat3.append(img[2*num1+i][j])
            mat2.append(mat3)
        mat1.append(mat2)

    mat1 = np.array(mat1)
    return mat1

################################################################################################################

def main():
    denoiserName = "median"
    dirname = "./" + denoiserName
    isdir = os.path.isdir(dirname)
    if isdir == False:
        os.mkdir(dirname)

    imgfile = "./aa.jpeg"
    img = cv2.imread(imgfile)
    stackedImage = stackImage(img)
    X = stackedImage
    dim = X.shape
    num1 = int(dim[0]/3)
    errnums = 1
    # Generate random noise and then, making the matrix sparse
    for errnum in range(errnums):
        errorname = "none"
        errormagnitude = []
        timetaken = []
        normdistances = []
        distances = []
        for i in range(1, 31):
            errmag = i
            Xerr = X
            # Random Sparse Noise
            if errnum == 0:
                A = np.random.randint(errmag, size=(dim[0], dim[1]))
                Err = lil_matrix(A).toarray()
                Xerr = X + Err
                Xerr = np.clip(Xerr, 0, 255)
                errorname = "random-sparse"
            # Gaussian Noise
            elif errnum == 1:
                mean = 0.0
                std = errmag
                Xerr = X + np.random.normal(mean, std, X.shape)
                Xerr = np.clip(Xerr, 0, 255)
                errorname = "gaussian"
            # Poisson Noise
            elif errnum == 2:
                vals = len(np.unique(Xerr))
                vals = 2 ** np.ceil(np.log2(vals)) * errmag
                Xerr = np.random.poisson(Xerr * vals) / float(vals)
                errorname = "poisson"
            # Speckle Noise
            elif errnum == 3:
                Err = errmag * np.random.randn(X.shape[0], X.shape[1])
                Err = Err.reshape(X.shape)
                Xerr = X + X * Err
                Xerr = np.clip(Xerr, 0, 255)
                errorname = "speckle"

            figure_size = 5
            begin = time.time()

            Xerr_R = Xerr[0:num1]
            Xerr_G = Xerr[num1:2*num1]
            Xerr_B = Xerr[2*num1:3*num1]

            Xerr_R = Xerr_R.astype(np.uint8)
            Xerr_G = Xerr_G.astype(np.uint8)
            Xerr_B = Xerr_B.astype(np.uint8)

            L_R = cv2.medianBlur(Xerr_R,figure_size)
            L_G = cv2.medianBlur(Xerr_G,figure_size)
            L_B = cv2.medianBlur(Xerr_B,figure_size)

            end = time.time()

            filteredImage_R = L_R.astype(np.uint8)
            filteredImage_G = L_G.astype(np.uint8)
            filteredImage_B = L_B.astype(np.uint8)

            filteredImage_R = L_R.astype(np.uint8)
            filteredImage_G = L_G.astype(np.uint8)
            filteredImage_B = L_B.astype(np.uint8)

            distr = np.linalg.norm((filteredImage_R-X[0:num1])/num1)
            distg = np.linalg.norm((filteredImage_G-X[num1:2*num1])/num1)
            distb = np.linalg.norm((filteredImage_R-X[2*num1:3*num1])/num1)
            errsum = (distr*distr + distg * distg + distb*distb)/3
            normdist = math.sqrt(errsum)

            errormagnitude.append(errmag)
            timetaken.append(end-begin)
            normdistances.append(normdist)

        filename = dirname + "/" + errorname + ".csv"
        dict1 = {'Error': errormagnitude, 'Time taken': timetaken, 'Distance': normdistances}
        df1 = pd. DataFrame(dict1)
        df1.to_csv(filename, mode='w', index=False)


if __name__ == '__main__':
    main()
