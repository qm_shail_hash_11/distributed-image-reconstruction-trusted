#!/bin/bash

# python code-rpca.py
# wait

python code-wiener.py
wait

python code-gaussian.py
wait

python code-mean.py
wait

python code-median.py
wait

python code-bilateral.py
wait
