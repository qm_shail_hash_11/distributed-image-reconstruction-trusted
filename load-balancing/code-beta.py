import random
import time
import sys
import pandas as pd
import os

def find_leader(monitor_points, sensor_point, betas):
    len1 = len(monitor_points)
    distances = []

    for i in range(len1):
        dist = find_distance(monitor_points[i], sensor_point)
        distances.append(dist)

    scores = get_score(distances, betas)

    min_val = min(scores)

    minmon = scores.index(min_val)

    return minmon

def find_distance(point1, point2):
    x = point1[0] - point2[0]
    y = point1[1] - point2[1]
    dist = x * x + y * y
    return dist

def get_score(distances, betas):
    len1 = len(distances)
    max_val = max(distances)
    scores = []
    for i in range(len1):
        score = distances[i] + max_val*betas[i]
        scores.append(score)
    return scores

def main(arg1, arg2):
    methodName = "beta"
    dirname = "./" + methodName
    isdir = os.path.isdir(dirname)
    if isdir == False:
        os.mkdir(dirname)

    random.seed(time.time())
    num1 = int(arg1)
    num2 = int(arg2)

    for k in range(5):

        betas = []

        monitor_points = []

        for i in range(num2):
            x1 = random.randint(1, 100)
            y1 = random.randint(1, 100)

            coords = (x1, y1)

            monitor_points.append(coords)

            betas.append(0)

        dictfinal = {}

        for i in range(num2):
            dictfinal[str(i)] = 0

        for i in range(num1):
            x1 = random.randint(1, 100)
            y1 = random.randint(1, 100)

            sensor_point = (x1, y1)

            leader = find_leader(monitor_points, sensor_point, betas)

            strleader = str(leader)

            betas[leader] = betas[leader] + 1

            dictfinal[strleader] = dictfinal[strleader] + 1

        df = pd.DataFrame(dictfinal, index=[0])
        df.to_csv(dirname + '/normal-'+ str(k) +'.csv', index=False)

if __name__ == '__main__':
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
    main(arg1, arg2)
