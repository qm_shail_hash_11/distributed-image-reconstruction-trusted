import random
import time
import sys
import pandas as pd

def find_leader(monitor_points, sensor_point):
    len1 = len(monitor_points)
    minmon = '0'
    mindist = find_distance(monitor_points[0], sensor_point)

    for i in range(len1):
        dist = find_distance(monitor_points[i], sensor_point)
        # print(i, dist)
        if dist < mindist:
            minmon = str(i)
            mindist = dist
    return minmon

def find_distance(point1, point2):
    x = point1[0] - point2[0]
    y = point1[1] - point2[1]
    dist = x * x + y * y
    return dist

def main(arg1, arg2):
    random.seed(time.time())
    num1 = int(arg1)
    num2 = int(arg2)

    for k in range(5):

        monitor_points = []

        for i in range(num2):
            x1 = random.randint(1, 100)
            y1 = random.randint(1, 100)

            coords = (x1, y1)

            monitor_points.append(coords)

        dictfinal = {}

        for i in range(num2):
            dictfinal[str(i)] = 0

        for i in range(num1):
            x1 = random.randint(1, 100)
            y1 = random.randint(1, 100)

            sensor_point = (x1, y1)

            leader = find_leader(monitor_points, sensor_point)

            # print(leader)

            dictfinal[leader] = dictfinal[leader] + 1

        df = pd.DataFrame(dictfinal, index=[0])
        df.to_csv('normal-'+ str(k) +'.csv', index=False)

if __name__ == '__main__':
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
    main(arg1, arg2)
