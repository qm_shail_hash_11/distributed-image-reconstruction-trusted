#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ptr.h"
#include "ns3/socket.h"
#include "ns3/packet.h"
#include "ns3/header.h"
#include "ns3/buffer.h"
#include "ns3/udp-header.h"
#include "ns3/udp-client.h"
#include <cryptopp/rsa.h>
#include <cryptopp/sha.h>
#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>
#include <cryptopp/files.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <iterator>
#include <stdio.h>
#include <vector>
#include <string>
#include <chrono> 
#include <ctime>
#include <cryptopp/md5.h>
#include <cryptopp/hex.h>
#include "ns3/node-container.h"
#include "ns3/ipv4-interface-container.h"
#include "ns3/ipv4-global-routing.h"
#include "ns3/ipv4.h"
#include "ns3/global-route-manager.h"
#include "ns3/global-router-interface.h"
#include "ns3/global-route-manager-impl.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#include "netapp.h"
#include <iostream>
#include <fstream>
#include "neth.h"

using namespace std;
using namespace ns3;
using namespace CryptoPP;

NetApp::NetApp ():
	v_nodes(),
	v_addresses(),
	v_ports(),
	mv_nodes(),
	mv_addresses(),
	mv_ports(),
	m_all_ids(),
	image_vector(),
	sensor_messages_vectors(),
	sensor_messages(),
	dimension(),
	image_id()
{
}

NetApp::~NetApp ()
{
}

TypeId 
NetApp::GetTypeId (void)
{
	static TypeId tid = TypeId ("NetApp")
	.SetParent<Application> ()
	.AddConstructor<NetApp> ()
	;
	return tid;
}

void
NetApp::Setup (std::vector<ns3::NodeContainer> vn, std::vector<ns3::Ipv4InterfaceContainer> vi, std::vector<std::vector<uint16_t>> vp, std::vector<ns3::NodeContainer> mvn, std::vector<ns3::Ipv4InterfaceContainer> mvi, std::vector<std::vector<uint16_t>> mvp, vector<string> mais, vector<vector<int>> imv, int dim, string iid)
{
	v_nodes = vn;
	v_addresses = vi;
	v_ports = vp;
	mv_nodes = mvn;
	mv_addresses = mvi;
	mv_ports = mvp;
	m_all_ids = mais;
	image_vector = imv;
	dimension = dim;
	image_id = iid;
}

void
NetApp::StartApplication (void)
{
	uint len = v_nodes.size();
	GenerateMessage(image_vector);

	for (uint i = 0; i < len; i++)
	{
		Ptr<Packet> pkt = CreatePacket(i);
		SendPacket(pkt, i);
	}
}

void
NetApp::StopApplication(void){
	// cout << "Stopped" << endl;
}

double
NetApp::calcDistance(std::vector<uint> point1, std::vector<uint> point2) {
	double par1 = point1.at(0) - point2.at(0);
	double par2 = point1.at(1) - point2.at(1);
	double dist = sqrt(par1*par1 + par2*par2);
	return dist;
}

uint
NetApp::closestPoint(std::vector<uint> point, std::vector<std::vector<uint>> mpoints) {
	double mindist = calcDistance(point, mpoints.at(0));
	uint minindex = 0;
	uint l = mpoints.size();
	for (uint i = 0; i < l; i++)
	{
		double dist = calcDistance(point, mpoints.at(i));
		if (dist < mindist)
		{
			mindist = dist;
			minindex = i;
		}
	}
	return minindex;
}

void
NetApp::GenerateMessage(vector<vector<int>> imv) {
	uint len = v_nodes.size();
	uint rows = imv.size();
	uint num1 = uint(rows/len);
	uint num2 = rows%len;
	vector<uint> number_of_rows;
	for (uint i = 0; i < len; i++ ) {
		uint val = num1;
		if(num2 != 0) {
			val = num1 + 1;
			num2 = num2 - 1;
		}
		number_of_rows.push_back(val);
	}

	// for (int i: number_of_rows) {
	// 	cout << i << endl;
	// }
	uint pos = 0;
	uint count = 0;
	vector<vector<int>> temp;
	for (uint i = 0; i < rows; i++) {
		temp.push_back(imv.at(i));
		count = count + 1;
		if(count == number_of_rows.at(pos)) {
			count = 0;
			pos = pos + 1;
			sensor_messages_vectors.push_back(temp);
			temp.clear();	
		}
	}

	for (uint i = 0; i < len; i++) {
		vector<vector<int>> element = sensor_messages_vectors.at(i);
		string mes = "{";
		for (uint j = 0; j < element.size(); j++) {
			mes = mes + "[";
			for (uint k = 0; k < element.at(j).size(); k++) {
				if (k != element.at(j).size() - 1) {
					mes = mes + to_string(element.at(j).at(k)) + ",";
				}
				else {
					mes = mes + to_string(element.at(j).at(k));
				}
			}
			mes = mes + "]";
			if (j != element.size() - 1) {
				mes = mes + ",";
			}
		}
		mes = mes + "}";
		sensor_messages.push_back(mes);
	}
}

Ptr<Packet>
NetApp::CreatePacket(uint pos)
{
	string packet_message = sensor_messages.at(pos);
	string monitor_id = m_all_ids.at(pos);
	uint len1 = image_id.length();
	uint len12 = monitor_id.length();
	uint len3 = packet_message.length();

	Ptr<Packet> pkt = Create<Packet> ();
	ns3::SensorHeader header;
	header.SetIDSize(len1);
	header.SetMIDSize(len12);
	header.SetMessageSize(len3);
	header.SetID(image_id);
	header.SetMID(monitor_id);
	header.SetMessage(packet_message);
	pkt->AddHeader(header);

	return pkt;
}

void
NetApp::SendPacket(Ptr<Packet> pkt, uint i)
{
	NodeContainer n_1 = v_nodes.at(i);
	Ipv4InterfaceContainer i_1 = v_addresses.at(i);
	std::vector<uint16_t> p_1 = v_ports.at(i);

	TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

	Ptr<Socket> soc1 = Socket::CreateSocket (n_1.Get(0), tid);
	InetSocketAddress add1 = InetSocketAddress (i_1.GetAddress(0), p_1.at(0));
	soc1->Bind (add1);

	Ptr<Socket> soc2 = Socket::CreateSocket (n_1.Get(1), tid);
	InetSocketAddress add2 = InetSocketAddress (i_1.GetAddress(1), p_1.at(1));
	soc2->Bind (add2);

	soc1->Connect (add2);
	soc1->Send(pkt);

	soc2->SetRecvCallback (MakeCallback (&NetApp::HandleRead, this));
}

void 
NetApp::HandleRead(Ptr<Socket> socket)
{
	Ptr<Packet> packet = socket->Recv ();
	Ptr<Packet> pkt = packet;
	SensorHeader header;
	pkt->RemoveHeader(header);

	string para1 = header.GetID();
	string para2 = header.GetMID();
	string para3 = header.GetMessage();

	uint len1 = para1.length();
	uint len2 = para2.length();
	uint len3 = para3.length();

	uint ind = para2.find("_");
	uint own_id = stoi(para2.substr(ind+1));

	MonitorImageHeader mheader;
	Ptr<Packet> mpkt = Create<Packet> ();
	mheader.SetIDSize(len1);
	mheader.SetMIDSize(len2);
	mheader.SetMessageSize(len3);
	mheader.SetID(para1);
	mheader.SetMID(para2);
	mheader.SetMessage(para3);
	mpkt -> AddHeader(mheader);

	uint reqlen = v_nodes.size();

	for (uint i = 0; i < reqlen; i++)
	{
		if (i == own_id)
		{
			continue;
		}

		vector<uint16_t> ps;
		ps.push_back(own_id+1);
		ps.push_back(i+1);
		auto it = find(mv_ports.begin(), mv_ports.end(), ps);
		int req_index = it - mv_ports.begin();
		NodeContainer n_1 = mv_nodes.at(req_index);
		Ipv4InterfaceContainer i_1 = mv_addresses.at(req_index);
		std::vector<uint16_t> p_1 = mv_ports.at(req_index);

		TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

		Ptr<Socket> soc1 = Socket::CreateSocket (n_1.Get(0), tid);
		InetSocketAddress add1 = InetSocketAddress (i_1.GetAddress(0), p_1.at(0));
		soc1->Bind (add1);

		Ptr<Socket> soc2 = Socket::CreateSocket (n_1.Get(1), tid);
		InetSocketAddress add2 = InetSocketAddress (i_1.GetAddress(1), p_1.at(1));
		soc2->Bind (add2);

		soc1->Connect (add2);
		soc1->Send(mpkt);

		soc2->SetRecvCallback (MakeCallback (&NetApp::HandleRead2, this));
	}
}

void 
NetApp::HandleRead2(Ptr<Socket> socket)
{
	Ptr<Packet> packet = socket->Recv ();
	Ptr<Packet> pkt = packet;
	ns3::MonitorImageHeader header;
	pkt->RemoveHeader(header);

	string para1 = header.GetMessage();
	para1 = para1 + "";
}