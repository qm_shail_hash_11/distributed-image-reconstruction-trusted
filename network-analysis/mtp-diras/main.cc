#include <fstream>
#include "ns3/callback.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ptr.h"
#include "ns3/socket.h"
#include "ns3/packet.h"
#include "ns3/header.h"
#include "ns3/buffer.h"
#include "ns3/udp-header.h"
#include "ns3/udp-client.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/command-line.h"
#include <cryptopp/rsa.h>
#include <cryptopp/sha.h>
#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>
#include <cryptopp/files.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iterator>
#include <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <iostream>
#include <unistd.h>
#include <string>
#include <cmath>
#include <fstream>
#include "ns3/ipv4-global-routing.h"
#include "ns3/ipv4.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/global-route-manager.h"
#include "ns3/global-router-interface.h"
#include "ns3/global-route-manager-impl.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#include "netapp.h"
#include "mnetapp.h"
#include "eigen3/Eigen/Dense"
#include "eigen3/Eigen/Core"

using namespace std;
using namespace ns3;

vector<vector<int>> generateImage(const int dimension) {
	vector<vector<int>> im;
	vector<int> row;
	int maxpix = 256;
	for (int i = 0; i < 3*dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			int pix = rand()%maxpix;
			row.push_back(pix);
		}
		im.push_back(row);
		row.clear();
	}
	return im;
}

int main(int argc, char *argv[])
{
	srand(time(0));

	struct stat info;

	uint n_monitor_nodes;
    uint n_sensor_nodes;
	int imageDimension;
	uint cycles;

    CommandLine cmd;
    cmd.AddValue("n_monitor_nodes", "Number of monitor nodes", n_monitor_nodes);
    cmd.AddValue("n_sensor_nodes", "Number of sensor nodes", n_sensor_nodes);
	cmd.AddValue("imageDimension", "Dimension of the image matrix", imageDimension);
	cmd.AddValue("cycles", "Number of images broadcast by one node", cycles);
    cmd.Parse (argc, argv);

	NodeContainer monitor_nodes;
	monitor_nodes.Create(n_monitor_nodes);

	NodeContainer sensor_nodes;
	sensor_nodes.Create(n_sensor_nodes);

	PointToPointHelper p2p;
	p2p.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
	p2p.SetChannelAttribute ("Delay", StringValue ("2ms"));

	InternetStackHelper internet;
	internet.Install (NodeContainer::GetGlobal ());

	Ipv4AddressHelper ipv4_n;
	ipv4_n.SetBase ("10.0.0.0", "255.255.255.0");

	// First of all, the connection among all the monitor nodes is defined.
	std::vector<NodeContainer> vector_mm_nodecontainers;
	std::vector<Ipv4InterfaceContainer> vector_mm_ipv4containers;
	std::vector<std::vector<uint16_t>> vector_mm_ports;
	for (uint i = 0; i < n_monitor_nodes; ++i)
	{
		for (uint j = 0; j < n_monitor_nodes; ++j)
		{
			std::vector<uint16_t> row;
			if (i != j)
			{
				NodeContainer n_links = NodeContainer (monitor_nodes.Get (i), monitor_nodes.Get (j));
				NetDeviceContainer n_devs = p2p.Install (n_links);
				Ipv4InterfaceContainer interfaces = ipv4_n.Assign (n_devs);
				ipv4_n.NewNetwork ();

				vector_mm_nodecontainers.push_back(n_links);
				vector_mm_ipv4containers.push_back(interfaces);
				row.push_back(i+1);
				row.push_back(j+1);
				vector_mm_ports.push_back(row);
			}
		}
	}

	vector<int> sensor_image_count;

	// Connection between the monitor and the sensor nodes
	std::vector<NodeContainer> vector_sm_nodecontainers;
	std::vector<Ipv4InterfaceContainer> vector_sm_ipv4containers;
	std::vector<std::vector<uint16_t>> vector_sm_ports;
	for (uint16_t i = 0; i < n_sensor_nodes; ++i)
	{
		sensor_image_count.push_back(0);
		for (uint16_t j = 0; j < n_monitor_nodes; ++j)
		{
			std::vector<uint16_t> row;
			NodeContainer n_links = NodeContainer (sensor_nodes.Get (i), monitor_nodes.Get (j));
			NetDeviceContainer n_devs = p2p.Install (n_links);
			Ipv4InterfaceContainer interfaces = ipv4_n.Assign (n_devs);
			ipv4_n.NewNetwork ();

			vector_sm_nodecontainers.push_back(n_links);
			vector_sm_ipv4containers.push_back(interfaces);
			row.push_back(i+1);
			row.push_back(j+1);
			vector_sm_ports.push_back(row);
		}
	}

	uint packetCount = 0;
	// These vectors are used for passing the nodes that need to be sent the packets
	std::vector<NodeContainer> r_vectornodecontainers;
	std::vector<Ipv4InterfaceContainer> r_vectoripv4containers;
	std::vector<std::vector<uint16_t>> r_vectorports;

	std::vector<std::vector<uint>> m_all_coords;
	std::vector<string> m_all_ids;
	string m_id;

	// The random coordinates generation
	for(uint i = 0; i < n_monitor_nodes; i++)
	{
		uint x_coord = rand();
		uint y_coord = rand();
		std::vector<uint> coords;
		coords.push_back(x_coord);
		coords.push_back(y_coord);
		for (uint j = 0; j < n_monitor_nodes; j++)
		{
			if (i == j)
				continue;
			r_vectornodecontainers.push_back(vector_mm_nodecontainers.at(i*(n_monitor_nodes-1)+1));
			r_vectoripv4containers.push_back(vector_mm_ipv4containers.at(i*(n_monitor_nodes-1)+1));
			r_vectorports.push_back(vector_mm_ports.at(i*(n_monitor_nodes-1)+1));
		}
		m_id = "id_" + to_string(i);
		Ptr<MNetApp> app = CreateObject<MNetApp> ();
		app->Setup (r_vectornodecontainers, r_vectoripv4containers, r_vectorports, coords, m_id);
		r_vectornodecontainers.at(0).Get (0)->AddApplication (app);
		packetCount = packetCount + 1;
		app->SetStartTime (Seconds (1.0));
		app->SetStopTime (Seconds (200.0));

		r_vectornodecontainers.clear();
		r_vectoripv4containers.clear();
		r_vectorports.clear();

		m_all_ids.push_back(m_id);
		m_all_coords.push_back(coords);
	}

	int imageCount = 0;

	uint broadcaster = 0;

	while(1)
	{
		string image_id = "image" + to_string(sensor_image_count.at(broadcaster)) + "_" + "sensor" + to_string(broadcaster);
		sensor_image_count.at(broadcaster) = sensor_image_count.at(broadcaster) + 1;
		// Random coordinates
		uint x_coord = rand();
		uint y_coord = rand();
		std::vector<uint> coords;
		coords.push_back(x_coord);
		coords.push_back(y_coord);
		for (uint i = broadcaster*(n_monitor_nodes); i < (broadcaster+1)*(n_monitor_nodes); ++i)
		{
			r_vectornodecontainers.push_back(vector_sm_nodecontainers.at(i));
			r_vectoripv4containers.push_back(vector_sm_ipv4containers.at(i));
			r_vectorports.push_back(vector_sm_ports.at(i));
		}

		vector<vector<int>> image = generateImage(imageDimension);
		// for (vector<int> i: image)
		// {
		// 	for (int j: i)
		// 	{
    	// 		cout << j << ' ';
		// 	}
		// 	cout << endl;
		// }

		Ptr<NetApp> app = CreateObject<NetApp> ();
		app->Setup (r_vectornodecontainers, r_vectoripv4containers, r_vectorports, vector_mm_nodecontainers, vector_mm_ipv4containers, vector_mm_ports, coords, m_all_coords, m_all_ids, image, imageDimension, image_id);
		r_vectornodecontainers.at(0).Get (0)->AddApplication (app);
		app->SetStartTime (Seconds (1.0));
		app->SetStopTime (Seconds (200.0));

		r_vectornodecontainers.clear();
		r_vectoripv4containers.clear();
		r_vectorports.clear();

		imageCount = imageCount + 1;
		broadcaster = broadcaster + 1;
		if(uint(imageCount)%(n_sensor_nodes) == 0)
		{
			broadcaster = 0;
		}

		if((imageCount/n_sensor_nodes) == cycles)
		{
			break;
		}


	}

	string init_dir = "./mtp";

    // Create the directory if not present
    if (stat(init_dir.c_str(), &info))
    {
        if (mkdir(init_dir.c_str(), 0777) == -1)
            cerr << "Error :  " << strerror(errno) << endl;
  
        else
        {
            cout << "Directory created" << endl;
            string perm_comm = "chmod a+rwx " + init_dir;
            system(perm_comm.c_str());
        }
    }

    else
    {
        cout << init_dir << " exists" << endl;
    }

	string parent_dir = init_dir + "/diras";

	if (stat(parent_dir.c_str(), &info))
    {
        if (mkdir(parent_dir.c_str(), 511) == -1)
            cerr << "Error :  " << strerror(errno) << endl;
        else
        {
            cout << "Directory created" << endl;
            string perm_comm = "chmod a+rwx " + parent_dir;
            system(perm_comm.c_str());
            //chmod(parent_dir.c_str(), S_IWUSR);
        }
	}


	auto filename = parent_dir + "/diras_monitor" + to_string(n_monitor_nodes) + "_sensor" + to_string(n_sensor_nodes) + "_size" + to_string(imageDimension) + ".xml";
	
	Ptr<FlowMonitor> flowMonitor;
	FlowMonitorHelper flowHelper;
	flowMonitor = flowHelper.InstallAll();

	Simulator::Stop (Seconds (205.0));
	Simulator::Run ();
	flowMonitor->SerializeToXmlFile(filename, true, true);

	Simulator::Destroy ();
	return 0;

}
