#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ptr.h"
#include "ns3/socket.h"
#include "ns3/packet.h"
#include "ns3/header.h"
#include "ns3/buffer.h"
#include "ns3/udp-header.h"
#include "ns3/udp-client.h"
#include <cryptopp/rsa.h>
#include <cryptopp/sha.h>
#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>
#include <cryptopp/files.h>
#include "cryptopp/cryptlib.h"
#include "cryptopp/sha.h"
#include <cryptopp/hex.h>
#include <iterator>
#include <stdio.h>
#include <vector>
#include <string>
#include "ns3/ipv4-global-routing.h"
#include "ns3/ipv4.h"
#include "ns3/global-route-manager.h"
#include "ns3/global-router-interface.h"
#include "ns3/global-route-manager-impl.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#include <cryptopp/rsa.h>
#include <cryptopp/sha.h>
#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>
#include <cryptopp/files.h>
#include "cryptopp/cryptlib.h"
#include "cryptopp/sha.h"
#include <cryptopp/hex.h>

#include "neth.h"

using namespace std;

namespace ns3{
	class MNetApp : public Application
	{
		public:
			MNetApp();
			~MNetApp();
			static TypeId GetTypeId (void);
			void Setup(std::vector<ns3::NodeContainer> vn, std::vector<ns3::Ipv4InterfaceContainer> vi, std::vector<std::vector<uint16_t>> vp, std::vector<uint> c, string mid);
			virtual void StartApplication (void);
			virtual void StopApplication (void);
			Ptr<Packet> CreatePacket();
			void SendPacket(Ptr<Packet> pkt, uint i);
			void HandleRead(Ptr<Socket> socket);
			std::vector<NodeContainer> v_nodes;
			std::vector< Ipv4InterfaceContainer> v_addresses;
			std::vector<std::vector<uint16_t>> v_ports;
	 		std::vector<uint> coordinates;
			string message;
			string monitor_id;
	};
}
