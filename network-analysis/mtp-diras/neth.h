#ifndef NET_HEADER_H
#define NET_HEADER_H

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/packet.h"
#include "ns3/header.h"
#include "ns3/buffer.h"
#include "ns3/ptr.h"
#include "ns3/udp-client.h"
#include <iterator>
#include <stdio.h>
#include <vector>
#include <string>
#include <chrono>
#include <ctime>
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>
#include <cryptopp/hex.h>
#include "ns3/ipv4-global-routing.h"
#include "ns3/ipv4.h"
#include "ns3/global-route-manager.h"
#include "ns3/global-router-interface.h"
#include "ns3/global-route-manager-impl.h"
#include "ns3/ipv4-address.h"

using namespace std;

namespace ns3{

	class MonitorHeader : public Header
	{
		public:
			MonitorHeader();
			~MonitorHeader();
			void SetCoordinates(string m);
			void SetID(string m);
			void SetCoordinatesSize(uint32_t n);
			void SetIDSize(uint32_t n);
			string GetCoordinates(void);
			string GetID(void);
			uint32_t GetCoordinatesSize(void);
			uint32_t GetIDSize(void);
			static TypeId GetTypeId (void);
			virtual TypeId GetInstanceTypeId (void) const;
			uint32_t GetSerializedSize(void) const;
			virtual void Serialize (Buffer::Iterator start) const;
			virtual uint32_t Deserialize (Buffer::Iterator start);
			virtual void Print (std::ostream &os) const;
		private:
			uint32_t m_id_size;
			uint32_t m_coords_size;
			string m_id;
			string m_coords;
	};

	class SensorHeader : public Header
	{
		public:
			SensorHeader();
			~SensorHeader();
			void SetCoordinates(string m);
			void SetID(string m);
			void SetMID(string m);
			void SetMessage(string m);
			void SetCoordinatesSize(uint32_t n);
			void SetIDSize(uint32_t n);
			void SetMIDSize(uint32_t n);
			void SetMessageSize(uint32_t n);
			string GetCoordinates(void);
			string GetID(void);
			string GetMID(void);
			string GetMessage(void);
			uint32_t GetCoordinatesSize(void);
			uint32_t GetIDSize(void);
			uint32_t GetMIDSize(void);
			uint32_t GetMessageSize(void);
			static TypeId GetTypeId (void);
			virtual TypeId GetInstanceTypeId (void) const;
			uint32_t GetSerializedSize(void) const;
			virtual void Serialize (Buffer::Iterator start) const;
			virtual uint32_t Deserialize (Buffer::Iterator start);
			virtual void Print (std::ostream &os) const;
		private:
			uint32_t m_id_size;
			uint32_t m_mid_size;
			uint32_t m_coords_size;
			uint32_t m_message_size;
			string m_id;
			string m_mid;
			string m_coords;
			string m_message;
	};

	class MonitorImageHeader : public Header
	{
		public:
			MonitorImageHeader();
			~MonitorImageHeader();
			void SetMID(string m);
			void SetID(string m);
			void SetMessage(string m);
			void SetMIDSize(uint32_t n);
			void SetIDSize(uint32_t n);
			void SetMessageSize(uint32_t n);
			string GetMID(void);
			string GetID(void);
			string GetMessage(void);
			uint32_t GetMIDSize(void);
			uint32_t GetIDSize(void);
			uint32_t GetMessageSize(void);
			static TypeId GetTypeId (void);
			virtual TypeId GetInstanceTypeId (void) const;
			uint32_t GetSerializedSize(void) const;
			virtual void Serialize (Buffer::Iterator start) const;
			virtual uint32_t Deserialize (Buffer::Iterator start);
			virtual void Print (std::ostream &os) const;
		private:
			uint32_t m_id_size;
			uint32_t m_mid_size;
			uint32_t m_message_size;
			string m_id;
			string m_mid;
			string m_message;
	};
}

#endif
