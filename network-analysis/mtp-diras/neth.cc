#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ptr.h"
#include "ns3/socket.h"
#include "ns3/packet.h"
#include "ns3/header.h"
#include "ns3/buffer.h"
#include "ns3/udp-header.h"
#include "ns3/udp-client.h"
#include <iterator>
#include <stdio.h>
#include <vector>
#include <string>
#include <chrono>
#include <ctime>
#include "ns3/ipv4-global-routing.h"
#include "ns3/ipv4.h"
#include "ns3/global-route-manager.h"
#include "ns3/global-router-interface.h"
#include "ns3/global-route-manager-impl.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>
#include <cryptopp/hex.h>

#include "neth.h"

using namespace std;
using namespace ns3;

//----------------------------------------------------------------------------------------------------------------------------------------------

MonitorHeader::MonitorHeader():
	m_id_size(0),
	m_coords_size(0),
	m_id(),
	m_coords()
{
}

MonitorHeader::~MonitorHeader()
{
}

TypeId
MonitorHeader::GetTypeId(void){
	static  TypeId tid = TypeId("MonitorHeader")
	.SetParent<Header>()
	.AddConstructor<MonitorHeader>()
	;
	return tid;
}

TypeId
MonitorHeader::GetInstanceTypeId(void) const{
	return GetTypeId();
}

void
MonitorHeader::SetCoordinates(string m)
{
	m_coords = m;
}

void
MonitorHeader::SetID(string m)
{
	m_id = m;
}

void
MonitorHeader::SetCoordinatesSize(uint32_t n)
{
	m_coords_size = n;
}

void
MonitorHeader::SetIDSize(uint32_t n)
{
	m_id_size = n;
}

uint32_t
MonitorHeader::GetSerializedSize(void)const
{
	uint32_t si = m_id_size + m_coords_size + 4 + 4;
	return si;
}

string
MonitorHeader::GetCoordinates(void)
{
	return m_coords;
}

string
MonitorHeader::GetID(void)
{
	return m_id;
}

uint32_t
MonitorHeader::GetCoordinatesSize(void)
{
	return m_coords_size;
}

uint32_t
MonitorHeader::GetIDSize(void)
{
	return m_id_size;
}

void
MonitorHeader::Serialize(Buffer::Iterator start) const{
	start.WriteHtonU32(m_id_size);
	start.WriteHtonU32(m_coords_size);

	std::vector<char> bytes1(m_id.begin(), m_id.end());
	for (uint32_t j=0; j< m_id_size ;j++)
	{
		start.WriteU8(bytes1[j]);
	}
	std::vector<char> bytes2(m_coords.begin(), m_coords.end());
	for (uint32_t j = 0; j < m_coords_size; j++ )
	{
		start.WriteU8(bytes2[j]);
	}
}

uint32_t
MonitorHeader::Deserialize (Buffer::Iterator start){
	m_id_size = start.ReadNtohU32();
	m_coords_size = start.ReadNtohU32();

	std::string str7;
	for (uint32_t j=0;j<m_id_size;j++)
	{
		str7.push_back(start.ReadU8 ());
	}
	m_id = str7;

	std::string str8;
	for (uint32_t j = 0; j < m_coords_size; j++)
	{
		str8.push_back(start.ReadU8());
	}
	m_coords = str8;

	return GetSerializedSize();
}

void
MonitorHeader::Print (std::ostream &os) const{
 	os << m_id << " " << m_coords << " " << endl;
}

// ---------------------------------------------------------------------------------------------------------------------------------------------

SensorHeader::SensorHeader():
	m_id_size(0),
	m_mid_size(0),
	m_coords_size(0),
	m_message_size(0),
	m_id(),
	m_mid(),
	m_coords(),
	m_message()
{
}

SensorHeader::~SensorHeader()
{
}

TypeId
SensorHeader::GetTypeId(void){
	static  TypeId tid = TypeId("SensorHeader")
	.SetParent<Header>()
	.AddConstructor<SensorHeader>()
	;
	return tid;
}

TypeId
SensorHeader::GetInstanceTypeId(void) const{
	return GetTypeId();
}

void
SensorHeader::SetCoordinates(string m)
{
	m_coords = m;
}

void
SensorHeader::SetID(string m)
{
	m_id = m;
}

void
SensorHeader::SetMID(string m)
{
	m_mid = m;
}

void
SensorHeader::SetMessage(string m)
{
	m_message = m;
}

void
SensorHeader::SetCoordinatesSize(uint32_t n)
{
	m_coords_size = n;
}

void
SensorHeader::SetIDSize(uint32_t n)
{
	m_id_size = n;
}

void
SensorHeader::SetMIDSize(uint32_t n)
{
	m_mid_size = n;
}

void
SensorHeader::SetMessageSize(uint32_t n)
{
	m_message_size = n;
}

uint32_t
SensorHeader::GetSerializedSize(void)const
{
	uint32_t si = m_id_size + m_mid_size + m_coords_size + m_message_size + 4 + 4 + 4 + 4;
	return si;
}

string
SensorHeader::GetCoordinates(void)
{
	return m_coords;
}

string
SensorHeader::GetID(void)
{
	return m_id;
}

string
SensorHeader::GetMID(void)
{
	return m_mid;
}

string
SensorHeader::GetMessage(void)
{
	return m_message;
}

uint32_t
SensorHeader::GetCoordinatesSize(void)
{
	return m_coords_size;
}

uint32_t
SensorHeader::GetIDSize(void)
{
	return m_id_size;
}

uint32_t
SensorHeader::GetMIDSize(void)
{
	return m_mid_size;
}

uint32_t
SensorHeader::GetMessageSize(void)
{
	return m_message_size;
}

void
SensorHeader::Serialize(Buffer::Iterator start) const{
	start.WriteHtonU32(m_id_size);
	start.WriteHtonU32(m_mid_size);
	start.WriteHtonU32(m_coords_size);
	start.WriteHtonU32(m_message_size);

	std::vector<char> bytes1(m_id.begin(), m_id.end());
	for (uint32_t j=0; j< m_id_size ;j++)
	{
		start.WriteU8(bytes1[j]);
	}
	std::vector<char> bytes12(m_mid.begin(), m_mid.end());
	for (uint32_t j=0; j< m_mid_size ;j++)
	{
		start.WriteU8(bytes12[j]);
	}
	std::vector<char> bytes2(m_coords.begin(), m_coords.end());
	for (uint32_t j = 0; j < m_coords_size; j++ )
	{
		start.WriteU8(bytes2[j]);
	}
	std::vector<char> bytes3(m_message.begin(), m_message.end());
	for (uint32_t j = 0; j < m_message_size; j++ )
	{
		start.WriteU8(bytes3[j]);
	}
}

uint32_t
SensorHeader::Deserialize (Buffer::Iterator start){
	m_id_size = start.ReadNtohU32();
	m_mid_size = start.ReadNtohU32();
	m_coords_size = start.ReadNtohU32();
	m_message_size = start.ReadNtohU32();

	std::string str7;
	for (uint32_t j=0;j<m_id_size;j++)
	{
		str7.push_back(start.ReadU8 ());
	}
	m_id = str7;

	std::string str72;
	for (uint32_t j=0;j<m_mid_size;j++)
	{
		str72.push_back(start.ReadU8 ());
	}
	m_mid = str72;

	std::string str8;
	for (uint32_t j = 0; j < m_coords_size; j++)
	{
		str8.push_back(start.ReadU8());
	}
	m_coords = str8;

	std::string str9;
	for (uint32_t j = 0; j < m_message_size; j++)
	{
		str9.push_back(start.ReadU8());
	}
	m_message = str9;

	return GetSerializedSize();
}

void
SensorHeader::Print (std::ostream &os) const{
 	os << m_id << " " << m_coords << " " << m_message << " " << endl;
}

// ---------------------------------------------------------------------------------------------------------------------------------------------

MonitorImageHeader::MonitorImageHeader():
	m_id_size(0),
	m_mid_size(0),
	m_message_size(0),
	m_id(),
	m_mid(),
	m_message()
{
}

MonitorImageHeader::~MonitorImageHeader()
{
}

TypeId
MonitorImageHeader::GetTypeId(void){
	static  TypeId tid = TypeId("MonitorImageHeader")
	.SetParent<Header>()
	.AddConstructor<MonitorImageHeader>()
	;
	return tid;
}

TypeId
MonitorImageHeader::GetInstanceTypeId(void) const{
	return GetTypeId();
}

void
MonitorImageHeader::SetMID(string m)
{
	m_mid = m;
}

void
MonitorImageHeader::SetID(string m)
{
	m_id = m;
}

void
MonitorImageHeader::SetMessage(string m)
{
	m_message = m;
}

void
MonitorImageHeader::SetMIDSize(uint32_t n)
{
	m_mid_size = n;
}

void
MonitorImageHeader::SetIDSize(uint32_t n)
{
	m_id_size = n;
}

void
MonitorImageHeader::SetMessageSize(uint32_t n)
{
	m_message_size = n;
}

uint32_t
MonitorImageHeader::GetSerializedSize(void)const
{
	uint32_t si = m_id_size + m_mid_size + m_message_size + 4 + 4 + 4;
	return si;
}

string
MonitorImageHeader::GetMID(void)
{
	return m_mid;
}

string
MonitorImageHeader::GetID(void)
{
	return m_id;
}

string
MonitorImageHeader::GetMessage(void)
{
	return m_message;
}

uint32_t
MonitorImageHeader::GetMIDSize(void)
{
	return m_mid_size;
}

uint32_t
MonitorImageHeader::GetIDSize(void)
{
	return m_id_size;
}

uint32_t
MonitorImageHeader::GetMessageSize(void)
{
	return m_message_size;
}

void
MonitorImageHeader::Serialize(Buffer::Iterator start) const{
	start.WriteHtonU32(m_id_size);
	start.WriteHtonU32(m_mid_size);
	start.WriteHtonU32(m_message_size);

	std::vector<char> bytes1(m_id.begin(), m_id.end());
	for (uint32_t j=0; j< m_id_size ;j++)
	{
		start.WriteU8(bytes1[j]);
	}
	std::vector<char> bytes2(m_mid.begin(), m_mid.end());
	for (uint32_t j = 0; j < m_mid_size; j++ )
	{
		start.WriteU8(bytes2[j]);
	}
	std::vector<char> bytes3(m_message.begin(), m_message.end());
	for (uint32_t j = 0; j < m_message_size; j++ )
	{
		start.WriteU8(bytes3[j]);
	}
}

uint32_t
MonitorImageHeader::Deserialize (Buffer::Iterator start){
	m_id_size = start.ReadNtohU32();
	m_mid_size = start.ReadNtohU32();
	m_message_size = start.ReadNtohU32();

	std::string str7;
	for (uint32_t j=0;j<m_id_size;j++)
	{
		str7.push_back(start.ReadU8 ());
	}
	m_id = str7;

	std::string str8;
	for (uint32_t j = 0; j < m_mid_size; j++)
	{
		str8.push_back(start.ReadU8());
	}
	m_mid = str8;

	std::string str9;
	for (uint32_t j = 0; j < m_message_size; j++)
	{
		str9.push_back(start.ReadU8());
	}
	m_message = str9;

	return GetSerializedSize();
}

void
MonitorImageHeader::Print (std::ostream &os) const{
 	os << m_id << " " << m_mid << " " << m_message << " " << endl;
}
