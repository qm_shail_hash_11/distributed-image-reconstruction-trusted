#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ptr.h"
#include "ns3/socket.h"
#include "ns3/packet.h"
#include "ns3/header.h"
#include "ns3/buffer.h"
#include "ns3/udp-header.h"
#include "ns3/udp-client.h"
#include <cryptopp/rsa.h>
#include <cryptopp/sha.h>
#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>
#include <cryptopp/files.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <iterator>
#include <stdio.h>
#include <vector>
#include <string>
#include <chrono>
#include <ctime>
#include <cryptopp/md5.h>
#include <cryptopp/hex.h>
#include "ns3/node-container.h"
#include "ns3/ipv4-interface-container.h"
#include "ns3/ipv4-global-routing.h"
#include "ns3/ipv4.h"
#include "ns3/global-route-manager.h"
#include "ns3/global-router-interface.h"
#include "ns3/global-route-manager-impl.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#include <iostream>
#include <fstream>
#include "neth.h"
#include "mnetapp.h"

using namespace std;
using namespace ns3;
using namespace CryptoPP;

MNetApp::MNetApp ():
    v_nodes(),
    v_addresses(),
    v_ports(),
	coordinates(),
    message(),
    monitor_id()
{
}

MNetApp::~MNetApp ()
{
}

TypeId
MNetApp::GetTypeId (void)
{
    static TypeId tid = TypeId ("MNetApp")
    .SetParent<Application> ()
    .AddConstructor<MNetApp> ()
    ;
    return tid;
}

void
MNetApp::Setup (std::vector<ns3::NodeContainer> vn, std::vector<ns3::Ipv4InterfaceContainer> vi, std::vector<std::vector<uint16_t>> vp, std::vector<uint> c, string mid)
{
    v_nodes = vn;
    v_addresses = vi;
    v_ports = vp;
    coordinates = c;
    string s1 = to_string(c.at(0));
    string s2 = to_string(c.at(1));
    string m1 = "(" + s1 + ", " + s2 + ")";
    message = m1;
    monitor_id = mid;
}

void
MNetApp::StartApplication (void)
{
    uint len = v_nodes.size();

    Ptr<Packet> pkt = CreatePacket();
	for (uint i = 0; i < len; i++)
	{
		SendPacket(pkt, i);
	}
}

void
MNetApp::StopApplication (void)
{
    
}

Ptr<Packet>
MNetApp::CreatePacket()
{
    uint32_t len1 = monitor_id.length();
    uint32_t len2 = message.length();

    Ptr<Packet> pkt = Create<Packet> ();
    ns3::MonitorHeader header;
    header.SetIDSize(len1);
    header.SetCoordinatesSize(len2);
    header.SetCoordinates(message);
    header.SetID(monitor_id);
    pkt->AddHeader(header);

    return pkt;
}

void
MNetApp::SendPacket(Ptr<Packet> pkt, uint i)
{
	NodeContainer n_1 = v_nodes.at(i);
	Ipv4InterfaceContainer i_1 = v_addresses.at(i);
	std::vector<uint16_t> p_1 = v_ports.at(i);

	TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

	Ptr<Socket> soc1 = Socket::CreateSocket (n_1.Get(0), tid);
	InetSocketAddress add1 = InetSocketAddress (i_1.GetAddress(0), p_1.at(0));
	soc1->Bind (add1);

	Ptr<Socket> soc2 = Socket::CreateSocket (n_1.Get(1), tid);
	InetSocketAddress add2 = InetSocketAddress (i_1.GetAddress(1), p_1.at(1));
	soc2->Bind (add2);

	soc1->Connect (add2);
	soc1->Send(pkt);

	soc2->SetRecvCallback (MakeCallback (&MNetApp::HandleRead, this));
}

void
MNetApp::HandleRead(Ptr<Socket> socket)
{
	Ptr<Packet> packet = socket->Recv ();
	Ptr<Packet> pkt = packet;
	MonitorHeader header;
	pkt->RemoveHeader(header);

	string para1 = header.GetCoordinates();
	string para2 = header.GetID();

    std::ofstream edgefile;
	string file_loc = "./edge.txt";
	edgefile.open(file_loc, std::ios_base::app);

	edgefile << "------------------------------------------------------------------------------------------------------------------" << "\n";
	edgefile << para1 << endl;
	edgefile << para2 << endl;
	edgefile << "------------------------------------------------------------------------------------------------------------------" << "\n";
	edgefile.close();
}
