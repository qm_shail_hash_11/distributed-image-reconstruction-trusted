#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ptr.h"
#include "ns3/socket.h"
#include "ns3/packet.h"
#include "ns3/header.h"
#include "ns3/buffer.h"
#include "ns3/udp-header.h"
#include "ns3/udp-client.h"
#include <cryptopp/rsa.h>
#include <cryptopp/sha.h>
#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>
#include <cryptopp/files.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <iterator>
#include <stdio.h>
#include <vector>
#include <string>
#include <chrono> 
#include <ctime>
#include <cryptopp/md5.h>
#include <cryptopp/hex.h>
#include "ns3/node-container.h"
#include "ns3/ipv4-interface-container.h"
#include "ns3/ipv4-global-routing.h"
#include "ns3/ipv4.h"
#include "ns3/global-route-manager.h"
#include "ns3/global-router-interface.h"
#include "ns3/global-route-manager-impl.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#include "netapp.h"
#include <iostream>
#include <fstream>
#include "neth.h"

using namespace std;
using namespace ns3;
using namespace CryptoPP;

NetApp::NetApp ():
	v_nodes(),
	v_addresses(),
	v_ports(),
	image_vector(),
	sensor_message(),
	dimension(),
	image_id()
{
}

NetApp::~NetApp ()
{
}

TypeId 
NetApp::GetTypeId (void)
{
	static TypeId tid = TypeId ("NetApp")
	.SetParent<Application> ()
	.AddConstructor<NetApp> ()
	;
	return tid;
}

void
NetApp::Setup (std::vector<ns3::NodeContainer> vn, std::vector<ns3::Ipv4InterfaceContainer> vi, std::vector<std::vector<uint16_t>> vp, vector<vector<int>> imv, const int dim, string iid)
{
	v_nodes = vn;
	v_addresses = vi;
	v_ports = vp;
	image_vector = imv;
	dimension = dim;
	image_id = iid;
}

void
NetApp::StartApplication (void)
{
	uint len = v_nodes.size();
	GenerateMessage(image_vector);
	Ptr<Packet> pkt = CreatePacket();

	for (uint i = 0; i < len; i++)
	{
		SendPacket(pkt, i);
	}
}

void
NetApp::StopApplication(void){
	// cout << "Stopped" << endl;
}

double
NetApp::calcDistance(std::vector<uint> point1, std::vector<uint> point2) {
	double par1 = point1.at(0) - point2.at(0);
	double par2 = point1.at(1) - point2.at(1);
	double dist = sqrt(par1*par1 + par2*par2);
	return dist;
}

uint
NetApp::closestPoint(std::vector<uint> point, std::vector<std::vector<uint>> mpoints) {
	double mindist = calcDistance(point, mpoints.at(0));
	uint minindex = 0;
	uint l = mpoints.size();
	for (uint i = 0; i < l; i++)
	{
		double dist = calcDistance(point, mpoints.at(i));
		if (dist < mindist)
		{
			mindist = dist;
			minindex = i;
		}
	}
	return minindex;
}

void
NetApp::GenerateMessage(vector<vector<int>> imv) {
	string s1 = "{";
	uint l1 = imv.size();
	for (uint i = 0; i < l1; i++) {
		uint l2 = imv.at(i).size();
		s1 = s1 + "[";
		for (uint j = 0; j < l2; j++) {
			if (j != l2 - 1) {
				s1 = s1 + to_string(imv.at(i).at(j)) + ",";
			}
			else {
				s1 = s1 + to_string(imv.at(i).at(j));
			}
		}
		s1 = s1 + "]";
		if (i != l1 - 1) {
			s1 = s1 + ",";
		}
	}
	s1 = s1 + "}";
	sensor_message = s1;
}

Ptr<Packet>
NetApp::CreatePacket()
{
	string packet_message = sensor_message;
	uint len1 = image_id.length();
	uint len3 = packet_message.length();

	Ptr<Packet> pkt = Create<Packet> ();
	ns3::SensorHeader header;
	header.SetIDSize(len1);
	header.SetMessageSize(len3);
	header.SetID(image_id);
	header.SetMessage(packet_message);
	pkt->AddHeader(header);

	return pkt;
}

void
NetApp::SendPacket(Ptr<Packet> pkt, uint i)
{
	NodeContainer n_1 = v_nodes.at(i);
	Ipv4InterfaceContainer i_1 = v_addresses.at(i);
	std::vector<uint16_t> p_1 = v_ports.at(i);

	TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

	Ptr<Socket> soc1 = Socket::CreateSocket (n_1.Get(0), tid);
	InetSocketAddress add1 = InetSocketAddress (i_1.GetAddress(0), p_1.at(0));
	soc1->Bind (add1);

	Ptr<Socket> soc2 = Socket::CreateSocket (n_1.Get(1), tid);
	InetSocketAddress add2 = InetSocketAddress (i_1.GetAddress(1), p_1.at(1));
	soc2->Bind (add2);

	soc1->Connect (add2);
	soc1->Send(pkt);

	soc2->SetRecvCallback (MakeCallback (&NetApp::HandleRead, this));
}

void 
NetApp::HandleRead(Ptr<Socket> socket)
{
	Ptr<Packet> packet = socket->Recv ();
	Ptr<Packet> pkt = packet;
	SensorHeader header;
	pkt->RemoveHeader(header);

	string para1 = header.GetMessage();
	para1 = para1 = "";
}