# DIRAS - Distributed Image Reconstruction - Trusted Monitor Nodes

DIRAS is a distributed framework for image reconstruction in CyberPhysical Systems (CPSs). This repository consists of the codes for various simulations.

## Network Analysis

The code is for ns-3 network simulator. It consists of codes for DIRAS, centralized model and decentralized model.

## Image Analysis

This part consists of:

1. Performance of RPCA and other image processing algorithms

2. Performance of RPCA and matrix completion together

3. Privacy analysis of DIRAS

## Load Balancing

This consists of the code for the load balancing proposed for improving DIRAS.
